import React from "react";
import { Image, View, StyleSheet, Text } from "react-native";
import ListItem from "../components/ListItem";
function ListingDetail(props) {
  return (
    <View>
      <Image style={styles.image} source={require("../assets/acoustic.jpg")} />
      <View style={styles.textContainer}>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.price}>{props.subtitle}</Text>
      </View>
      <View style={styles.userInfo}>
        <ListItem image={props.image} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 350,
  },
  textContainer: {
    padding: 20,
  },
  title: {
    fontSize: "bold",
    fontSize: 25,
  },
  price: {
    fontSize: 18,
    fontWeight: "800",
    color: "#FF7F50",
  },
  userInfo: {
    marginVertical: 50,
  },
});

export default ListingDetail;
