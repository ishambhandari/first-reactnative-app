import React from 'react';
import { FlatList, View } from 'react-native';
import ItemSeperatorLine from '../components/ItemSeperatorLine';
import ListItem from '../components/ListItem';
import ListItemDelete from '../components/ListItemDelete';




const messages = [
    {
        id: 1,
        title: "title1",
        description: "description1",
        image: require('../assets/logo.png')
    },
    {
        id: 2,
        title: "title2",
        description: "description2",
        image: require('../assets/logo.png')
    },


]
function MessageScreen(props) {
    return (

        <FlatList
            data={messages}
            keyExtractor={message => message.id.toString()}
            renderItem={({ item }) => <ListItem title={item.title} subtitle={item.description} image={item.image} onPress={() => console.log} renderRightActions={ListItemDelete} />}
            ItemSeparatorComponent={ItemSeperatorLine}
        />

    );
}

export default MessageScreen;