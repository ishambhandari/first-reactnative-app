import React from "react";
import { Image, StyleSheet, View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

function ViewImage(props) {
  return (
    <View styles={styles.container}>
      <View style={styles.close}>
        <MaterialCommunityIcons name="close" size={40} />
      </View>
      <View style={styles.delete}>
        <MaterialCommunityIcons name="delete" size={40} />
      </View>
      <Image
        resizeMode="contain"
        source={require("../assets/background.jpg")}
        style={styles.image}
      ></Image>
    </View>
  );
}

export default ViewImage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
  },
  image: {
    width: "100%",
    height: "100%",
  },

  close: {
    position: "absolute",
    top: 40,
    left: 40,
  },

  delete: {
    position: "absolute",
    top: 40,
    right: 30,
  },
});
