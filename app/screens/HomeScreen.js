import React from "react";
import { Image, ImageBackground, StyleSheet, Text, View } from "react-native";
import AppButtons from "../components/AppButtons";

function HomeScreen(props) {
  return (
    <ImageBackground
      blurRadius={1}
      source={require("../assets/background.jpg")}
      style={styles.background}
    >
      <View style={styles.topContainer}>
        <Image source={require("../assets/logo.png")} style={styles.logo} />
        <Text style={styles.moto}>Sell your instrument.</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <AppButtons
          color="#fc5c65"
          title="login"
          onPress={() => console.log("pressed")}
        />
        <AppButtons
          title="Register"
          color="#4ecdc4"
          onPress={() => console.log("pressed")}
        />
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    height: "100%",
    width: "100%",
  },
  logo: {
    width: 100,
    height: 100,
  },

  topContainer: {
    position: "absolute",
    top: 70,
    alignItems: "center",
  },
  buttonsContainer: {
    padding: 25,
    width: "100%",
  },
  moto: {
    fontSize: 25,
    fontWeight: "bold",
    paddingVertical: 18,
  },
});
export default HomeScreen;
