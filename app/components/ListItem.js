import React from "react";
import { View, StyleSheet, Image, Text, TouchableHighlight } from "react-native";
import Swipeable from 'react-native-gesture-handler/Swipeable'

function ListItem(props) {
  return (
    <Swipeable renderRightActions={props.renderRightActions}>
    <TouchableHighlight underlayColor="#f8f4f4" onPress={props.onPress}>
    <View style={styles.container}>
      <Image style={styles.image} source={props.image} />
      <View style={styles.textContainer}>
        <Text style={styles.name}>{props.title}</Text>
        <Text style={styles.listing}>{props.subtitle}</Text>
      </View>
    </View>
    </TouchableHighlight>
  </Swipeable>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding:15,
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 35,
    marginRight: 10,
  },
  textContainer: {
    flexDirection: "column",
  },
  name: {
    fontWeight: "800",
    padding: 5,
  },
  listing: {
    color: "#6e6969",
  },
});

export default ListItem;
