import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";
function Cards(props) {
  return (
    <View style={styles.card}>
      <Image style={styles.image} source={props.image}></Image>
      <View style={styles.textContainer}>
        <Text styles={styles.title}>{props.title}</Text>
        <Text styles={styles.title}>{props.subTitle}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 25,
    backgroundColor: "#fff",
    margin: 20,
    overflow: "hidden",
  },
  image: {
    width: "100%",
    height: 200,
  },
  textContainer: {
    padding: 20,
  },
  title: {
    marginBottom: 8,
  },
  subTitle: {
    color: "#4ecdc4",
    fontWeight: "bold",
  },
});

export default Cards;
